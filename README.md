### The project is still under development... Not that it will ever come with promise of function or quality anyways ;).

# expositor.io

> ex·pos·i·tor /ikˈspäzədər/
> 
> a person or thing that explains complicated ideas or theories.

Welcome to expositor.io! This project is intended to be an easy to use self hosted blog for developers to share ideas - particularly for other developers. Written in Django, deployed to AWS, it is a great home to get a jumpstart to a public presence while sharing with the community. 

More specifically the project aims to:
- Use and explain the chosen best practices for different scenarios (generally availability vs cost)
- Point out ideal places to implement new ideas
- Allow your blog readers to run examples in line with your explanations using jupyter
- Leverage aws resources for easy to use and low cost implementations
  - Point out billing considerations, cost savings, and improvement opportunities 

All you need to bring is an aws account!

> we are not responsible for your aws charges AT ALL. We are aiming to provide useful templates but you should fully understand and review what the code is doing before you use it.

# Getting started:

Getting started is as easy as cloning the repository, configuring the details, and launching it using a setup script. I suggest reviewing the roadmap (below) to understand what your options are available, in pilot, out soon, and more project information. That said, there's a lot of variables to consider so let's walk through it.

First, clone the project:

> git clone git@gitlab.com:code-in-pod/expositor.io.git

Next, consider your requirements by running getting-started.sh. This will walk you through the questions you need to answer to run the project successfully.

> ./getting-started.sh

This generates "my-expositor-config.json" which will be consumed by the deployment step.

Finally, launch the deployment. All parameters besides AWS credentials are technically optional but our defaults might not meet your tastes for cost or need for availability.

See [Dependencies]

> python deploy-expositor.py

The project roadmap will be a very important and honest document which describes what the project can be used for confidently and when. There will likely be a lot of initial constraints regarding the toolchain we are using.


# Roadmap

We're still figuring a lot out.

## Platforms

Platform wise we're looking at deploying web/app to AWS EKS. A K8s solution makes it fairly portable and least managed. An elastic beanstalk deployment might be on the short list of next features to add as it doesn't have a cost for the 'management' layer at all which is highly desireable. ECS has similar appeals but might not have self-patching host options, will need to double check those pros/cons.

We plan on running postgres on AWS RDS because it is quite easy and fairly cheap. The app won't have a lot of database activity ideally so a small instance should suffice.

An option for all-in-one likely won't be made available and if it ever is would only be for development/testing purposes.

## CICD

We plan to offer a well tested battle hardened pipeline for this application in at least gitlab ci. We will aim to make those steps as portable as possible.

## Application

The application itself will be a blog site first and foremost for user functionality. Additionally, the repository will offer a template to deploy a best practice environment with great documentation. Then comes additional blog features. We have plans for social media integration (facebook, twitter, youtube, linkedin, etc) to pull in a users latest activity to share with their followers. This can help those who work across multiple mediums when producing content. Secondly we aim to add developer tools to the platform first and foremost something like jupyter notebooks to enable embedded code execution for the users' users to follow along code examples with. We plan to continue tailoring the experience to developers looking to learn and share their learnings.

# Walkthrough

this is a walkthrough

## building blocks

### build

this is a talk through the build processes and how to extend it

### integration

this is a talk through testing and how to extend it

### deploy

this is a talk through k8s setup / other setup / target envs and how to extend it

### using your expositor.io

this is a talk about basic use of the applciation and how to extend it

# The Team

So there's Ian, Galya, and myself. We worked hard to bring you this mostly as practice, because we love learning, and becuase we thought it might make a fun portfolio project.



# Issue Tracking


For now, just look at trello:
https://trello.com/b/FivwQx3N/expositorio

After initial 1.0 release we will use gitlab issues for tracking, but no promises we maintain it much.


# Development Notes
## Expositor.io

```
docker container run --publish 5432:5432 --name expositor --detach --env POSTGRES_PASSWORD=password postgres
# delay while container starts up...
docker container exec --user postgres expositor psql --command='CREATE DATABASE expositordb;'
python manage.py makemigrations && python manage.py migrate
export DJANGO_SUPERUSER_PASSWORD=tiwinner322
python manage.py createsuperuser --username jdix531@gmail.com --email jdix531@gmail.com --no-input
docker container exec --user postgres expositor psql --dbname expositordb --command="UPDATE auth_user SET first_name='Josh',last_name='Dix' where id=1;"
```

other notes:
on ubuntu (container, at least) apt package "libpq-dev" (mac "libpq") was required in order to install psycopg2

Tests are organized where top level project tests exist in "./tests". Django-Application specific tests are located in "./<APP>/tests/".