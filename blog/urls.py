from django.urls import path
from django.views.generic import TemplateView
import blog.views

urlpatterns = [
    path('', TemplateView.as_view(template_name="blog.html"), name="blog")
]
